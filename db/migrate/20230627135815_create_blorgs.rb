class CreateBlorgs < ActiveRecord::Migration[7.0]
  def change
    create_table :blorgs do |t|
      t.belongs_to :time_start, null: false, foreign_key: true
      t.string :attr_start

      t.timestamps
    end
  end
end
