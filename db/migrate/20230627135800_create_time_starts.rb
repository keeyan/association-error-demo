class CreateTimeStarts < ActiveRecord::Migration[7.0]
  def change
    create_table :time_starts do |t|

      t.timestamps
    end
  end
end
